package com.treelineinteractive.recruitmenttask.data.network.model

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.widget.Toast
import androidx.core.content.ContextCompat.startActivity
import java.lang.StringBuilder
import java.time.LocalDate

class ProductItemSalesReport(val productItemSales: List<ProductItemSales>) {
    fun sendAsEmail(context: Context) {
        val intent = Intent(Intent.ACTION_SENDTO).apply {
            data = Uri.parse("mailto:")
            putExtra(Intent.EXTRA_EMAIL, arrayOf("bossman@bosscompany.com"))
            putExtra(Intent.EXTRA_SUBJECT, "Sales Report for %s".format(LocalDate.now()))
            putExtra(Intent.EXTRA_TEXT, generate())
        }
        try {
            startActivity(context, Intent.createChooser(intent, "Choose Email Client..."), null)
        }
        catch (e: Exception){
            Toast.makeText(context, e.message, Toast.LENGTH_LONG).show()
        }
    }

    private fun generate(): String {
        val stringBuilder = StringBuilder()
        var anySalesToday = false
        stringBuilder.append("Sales Report for %s".format(LocalDate.now())).append("\n\n")
        for (sale in productItemSales) {
            if (sale.sold > 0) {
                anySalesToday = true
                stringBuilder
                    .append(saleToString(sale))
                    .append("\n")
            }
        }
        if (!anySalesToday) {
            stringBuilder.append("No Sales today.").append("\n")
        }
        return stringBuilder.toString()
    }

    private fun saleToString(sale: ProductItemSales): String {
        val stringBuilder = StringBuilder()
        stringBuilder
            .append("ITEM: ").append(sale.item.title).append("\n")
            .append("ID: ").append(sale.item.id).append("\n")
            .append("SOLD: ").append(sale.sold).append("\n")

        return stringBuilder.toString()
    }
}