package com.treelineinteractive.recruitmenttask.data.network.model

import kotlin.properties.Delegates

data class ProductItemSales(
    val item: ProductItem,
    var sold: Int = 0
)
