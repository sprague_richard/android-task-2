package com.treelineinteractive.recruitmenttask.ui.view

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import com.google.android.material.R
import com.google.android.material.card.MaterialCardView
import com.treelineinteractive.recruitmenttask.data.network.model.ProductItem
import com.treelineinteractive.recruitmenttask.data.network.model.ProductItemSales
import com.treelineinteractive.recruitmenttask.databinding.ViewProductItemBinding
import kotlin.properties.Delegates

class ProductItemSalesView @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = R.attr.materialCardViewStyle
) : MaterialCardView(context, attrs, defStyleAttr) {

    private val binding  = ViewProductItemBinding.inflate(LayoutInflater.from(context), this)

    fun setProductItemSales(productItem: ProductItemSales) {
        binding.nameLabel.text = productItem.item.title
        binding.descriptionLabel.text = productItem.item.description
        binding.typeLabel.text = "Type: %s".format(productItem.item.type)
        binding.colorLabel.text = "Color: %s".format(productItem.item.color)
        binding.costLabel.text = "$%.2f".format(productItem.item.cost)
        updateSoldAndAvailableLabels(productItem)

        binding.sellMinusButton.setOnClickListener {
            if (productItem.sold > 0) {
                productItem.sold--
                updateSoldAndAvailableLabels(productItem)
            }
        }
        binding.sellPlusButton.setOnClickListener {
            if (productItem.sold < productItem.item.available) {
                productItem.sold++
                updateSoldAndAvailableLabels(productItem)
            }
        }
    }

    // TODO it'd be better to observe changes in the productItem data to update these labels
    fun updateSoldAndAvailableLabels(productItem: ProductItemSales) {
        binding.soldLabel.text = "Sold: " + productItem.sold
        binding.availableLabel.text =
            "Available: %d".format(productItem.item.available - productItem.sold)
    }
}