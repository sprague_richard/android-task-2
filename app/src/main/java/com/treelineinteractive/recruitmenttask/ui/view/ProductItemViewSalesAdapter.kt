package com.treelineinteractive.recruitmenttask.ui.view

import android.view.ViewGroup
import android.widget.LinearLayout
import androidx.recyclerview.widget.RecyclerView
import com.treelineinteractive.recruitmenttask.data.network.model.ProductItemSales

class ProductItemViewSalesAdapter(private val itemList : List<ProductItemSales>) : RecyclerView.Adapter<ProductItemViewSalesAdapter.ViewHolder>() {
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ViewHolder {
        val itemView = ProductItemSalesView(parent.context)
        val params = LinearLayout.LayoutParams(
            LinearLayout.LayoutParams.MATCH_PARENT,
            LinearLayout.LayoutParams.WRAP_CONTENT
        )
        params.topMargin = 16
        params.marginEnd = 16
        params.marginStart = 16
        itemView.layoutParams = params

        return ViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val salesItem: ProductItemSales = itemList[position]
        val itemView: ProductItemSalesView = holder.itemView as ProductItemSalesView
        itemView.setProductItemSales(salesItem)
    }

    override fun getItemCount(): Int = itemList.size

    class ViewHolder(itemView: ProductItemSalesView) : RecyclerView.ViewHolder(itemView)
}