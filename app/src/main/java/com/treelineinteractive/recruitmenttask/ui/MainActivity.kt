package com.treelineinteractive.recruitmenttask.ui

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import androidx.lifecycle.LiveData
import androidx.recyclerview.widget.LinearLayoutManager
import com.treelineinteractive.recruitmenttask.data.network.model.ProductItemSales
import com.treelineinteractive.recruitmenttask.data.network.model.ProductItemSalesReport
import com.treelineinteractive.recruitmenttask.databinding.ActivityMainBinding
import com.treelineinteractive.recruitmenttask.ui.view.ProductItemViewSalesAdapter

class MainActivity : AppCompatActivity() {

    private val binding by viewBinding(ActivityMainBinding::inflate)
    private val mainViewModel = MainViewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)

        mainViewModel.loadProducts()

        binding.retryButton.setOnClickListener {
            mainViewModel.loadProducts()
        }

        binding.itemsLayout.layoutManager =
            LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)

        mainViewModel.stateLiveData.observe { state ->
            binding.progressBar.isVisible = state.isLoading
            binding.errorLayout.isVisible = state.error != null
            binding.errorLabel.text = state.error

            binding.itemsLayout.isVisible = state.isSuccess
            binding.reportButton.isVisible = state.isSuccess

            if (state.isSuccess) {
                binding.itemsLayout.removeAllViews()
                val productItemSalesList = state.items.map { ProductItemSales(it) }

                binding.itemsLayout.adapter =
                    ProductItemViewSalesAdapter(productItemSalesList)

                binding.reportButton.setOnClickListener {
                    ProductItemSalesReport(productItemSalesList).sendAsEmail(this)
                }
            }
        }
    }

    fun <T> LiveData<T>.observe(onChanged: (T) -> Unit) {
        observer(this@MainActivity, onChanged)
    }
}